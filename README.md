# EnumeState

The tool that allows you to enumerate information about states!

# Assumptions

In writing this tool I have had to make some assumptions in the implementation.  It turns out there are several data sets to choose from in the API.
I decided the best course of action is to let the user choose which data set they would like to use.  By default the most recent data set is used.
All floating point values have been printed with 4 digits after the decimal place to provide enough precision.  It is also possible that the user
may or may not want headers in their CSV file so that has been made optional.  When printing the averages value a floating point number is printed
even though in the instructions it says to return an integer.  Go does (surprisingly) not have a rounding function and I didn't feel like writing one.
I preferred to round up rather than multiply by 100 and truncate everything after the decimal place and since I can't do that I chose to leave the
value as a floating point number.  Another interesting thing to note is that the documentation for demographic (state) API is wrong.  It does not
say that you can use "state" as a geography type though I figured out that you in fact can and that is what I used.  When generating a CSV file
using this tool it will only print to the screen.  The assumption there is that the user knows that can redirect standard output to a file if they
wish to do so.  The final assumption is that there is only one state each time the FIPS API is queried. The API only requires that you provide
3 letters to identify a state.  In the case of names like New York and New Mexico there is ambiguity if you provide the state name "new".
So, the code has been written so that if there are multiple states returns in a single query for a FIPS number only the first state in the list
will be used.  In order to get 2 states with similar names you need to provide enough characters per name in your states list to disambiguate these values.

# Building

Make sure that you have a Go compiler installed and check this repository out into your GOPATH.

```bash
$ mkdir -p gopath/{bin,pkg,src/github.com/mspaulding06}
$ cd gopath/src/github.com/mspaulding
$ git clone https://github.com/mspaulding06/enumestate.git
$ cd enumestate
$ make
```

Now the tool is installed into your $GOPATH/bin directory.

# Testing

After you have cloned the repository just run the tests.

```bash
$ go test
```

# Usage

View help with the `-h` option.

```bash
Usage of C:\Sandbox\gopath\bin\enumestate.exe:
  -baseurl string
        the API base URL to use (default "https://www.broadbandmap.gov/broadbandmap")
  -dataversion string
        data version, one of: jun2011, dec2011, jun2012, dec2012, jun2013, dec2013, jun2014 (default "jun2014")
  -header
        if using CSV output do you want a header?
  -output string
        output type, one of: CSV, averages (default "CSV")
  -states string
        comma separated list of states with no spaces
```

# Examples

```bash
$ enumestate -states california,oregon,washington
```

```bash
$ enumestate -states "district,new york" -output averages
```

# Vagrant

If you prefer to use Vagrant to install the tool just run the following

```bash
$ vagrant up
$ vagrant ssh
invagrant$ enumestate -states "new york,new mexico"
```
