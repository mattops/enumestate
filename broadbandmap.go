package enumestate

import (
    "encoding/json"
    "fmt"
    "io/ioutil"
    "log"
    "net/http"
    "net/url"
    "strings"
)

// BroadbandMapAPI represents an api object
type BroadbandMapAPI struct {
    baseURL string
}

// NewAPI create a new broadband map API object
func NewAPI(baseURL string) *BroadbandMapAPI {
    return &BroadbandMapAPI{
        baseURL: baseURL,
    }
}

// Query will query the API and return a Census object
func (bm BroadbandMapAPI) Query(dataVersion string, states []string) *Census {
    var fips []string
    for _, state := range states {
        fips = append(fips, bm.queryFIPS(state))
    }
    return bm.queryStates(dataVersion, fips)
}

func (bm *BroadbandMapAPI) queryEndpoint(endpoint string) []byte {
    apiURL := bm.baseURL + endpoint + "?format=json"
    // don't store the struct just check that it parses
    _, err := url.Parse(apiURL)
    if err != nil {
        log.Fatal("unable to parse endpoint url: ", apiURL)
    }
    resp, err := http.Get(apiURL)
    if err != nil {
        log.Fatal("failed to get endpoint: ", err)
    }
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        log.Fatal("unable to parse body: ", err)
    }
    return body
}

func (bm BroadbandMapAPI) queryFIPS(state string) string {
    endpoint := fmt.Sprintf("/census/state/%s/", state)
    content := bm.queryEndpoint(endpoint)
    var fipsContent map[string]interface{}
    json.Unmarshal(content, &fipsContent)
    // yes, this is brittle, we are hoping the data structure doesn't change
    status := fipsContent["status"].(string)
    if status == "Bad Request" {
        log.Fatal("bad request: ", fipsContent["message"].([]interface{})[0])
    }
    results := fipsContent["Results"].(map[string]interface{})
    data := results["state"].([]interface{})
    if len(data) < 1 {
        log.Fatal("unable to find state: ", state)
    }
    return data[0].(map[string]interface{})["fips"].(string)
}

func (bm BroadbandMapAPI) queryStates(dataVersion string, fips []string) *Census {
    fipsNums := strings.Join(fips, ",")
    endpoint := fmt.Sprintf("/demographic/%s/state/ids/%s/", dataVersion, fipsNums)
    content := bm.queryEndpoint(endpoint)
    var demoContent map[string]interface{}
    json.Unmarshal(content, &demoContent)
    results := demoContent["Results"].([]interface{})
    c := NewCensus()
    for _, item := range results {
        state := item.(map[string]interface{})
        row := &CensusRow{
            State:        state["geographyName"].(string),
            Population:   state["population"].(float64),
            Households:   state["households"].(float64),
            IncomeBP:     state["incomeBelowPoverty"].(float64),
            MedianIncome: state["medianIncome"].(float64),
        }
        c.AddRow(row)
    }
    return c
}