package enumestate

import (
    "fmt"
)

// CensusRow represents a single row in a CSV file
type CensusRow struct {
    State        string
    Population   float64
    Households   float64
    IncomeBP     float64
    MedianIncome float64
}

func (row CensusRow) String() string {
    return fmt.Sprintf("%s, %.0f, %.0f, %.4f, %.4f\n",
        row.State,
        row.Population,
        row.Households,
        row.IncomeBP,
        row.MedianIncome,
    )
}