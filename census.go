package enumestate

import "sort"

// Census represents a Census object
type Census struct {
    header bool
    rows   []*CensusRow
}

// NewCensus creates a new Census object
func NewCensus() *Census {
    return &Census{}
}

// AddRow will add a single row to the CSV object
func (c *Census) AddRow(row *CensusRow) {
    c.rows = append(c.rows, row)
}

// Rows returns the number of rows in the census data
func (c Census) Rows() int {
    return len(c.rows)
}

// CSV will convert the census data into a CSV text
func (c *Census) CSV(header bool) string {
    output := ""
    if header {
        output += "State, Population, Households, IncomeBelowPoverty, MedianIncome\n"
    }
    sort.Sort(ByName(c.rows))
    for _, row := range c.rows {
        output += row.String()
    }
    return output
}

// Averages will return a weighted average of the income below poverty
func (c Census) Averages() float64 {
    var n, d float64
    for _, row := range c.rows {
        n += row.Population * row.IncomeBP
        d += row.Population
    }
    return n / d
}
