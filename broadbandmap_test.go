package enumestate

import (
    "strings"
    "testing"
)

const baseURL = "https://www.broadbandmap.gov/broadbandmap"

var api = NewAPI(baseURL)

func TestFIPS(t *testing.T) {
    if api.queryFIPS("california") != "06" {
        t.Error("California's FIPS ID is 06")
    }
    if api.queryFIPS("district") != "11" {
        t.Error("D.C.'s FIPS ID is 11")
    }
}

func TestStates(t *testing.T) {
    c := api.queryStates("jun2014", []string{"06", "11"})
    if c.Rows() != 2 {
        t.Error("Census data should contain 2 rows")
    }
}

func TestAverages(t *testing.T) {
    c := api.Query("jun2014", []string{"oregon"})
    avg := c.Averages()
    if avg > 0.20 {
        t.Error("Value is too large")
    }
}

func TestCSV(t *testing.T) {
    c := api.Query("jun2014", []string{"new york", "ohio", "texas"})
    content := c.CSV(true)
    // this should be 5 since there is a final line return
    if len(strings.Split(content, "\n")) != 5 {
        t.Error("Incorrect number of rows in CSV")
    }
}
