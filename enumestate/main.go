package main

import (
    "flag"
    "fmt"
    "log"
    "strings"
    "github.com/mspaulding06/enumestate"
)

const baseURL = "https://www.broadbandmap.gov/broadbandmap"
var dataVersions = []string{
    "jun2011",
    "dec2011",
    "jun2012",
    "dec2012",
    "jun2013",
    "dec2013",
    "jun2014",
}

func main() {
    states := flag.String("states", "", "comma separated list of states with no spaces")
    output := flag.String("output", "CSV", "output type, one of: CSV, averages")
    dataVersion := flag.String("dataversion", "jun2014", fmt.Sprintf("data version, one of: %s", strings.Join(dataVersions, ", ")))
    baseURL := flag.String("baseurl", baseURL, "the API base URL to use")
    header := flag.Bool("header", false, "if using CSV output do you want a header?")
    flag.Parse()

    var stateList []string
    if *states == "" {
        log.Fatal("you must provide a list of states")
    } else {
        stateList = strings.Split(*states, ",")
    }

    foundVersion := false
    for _, v := range dataVersions {
        if v == *dataVersion {
            foundVersion = true
            break
        }
    }
    if !foundVersion {
        log.Fatal("data version is unknown: ", *dataVersion)
    }

    api := enumestate.NewAPI(*baseURL)
    c := api.Query(*dataVersion, stateList)
    if *output == "CSV" {
        fmt.Println(c.CSV(*header))
    } else if *output == "averages" {
        fmt.Printf("%.4f\n", c.Averages())
    } else {
        log.Fatal("must supply output option of CSV or averages")
    }
}