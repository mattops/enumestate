package enumestate

// ByName is used to sort rows
type ByName []*CensusRow

// Len function for sorting
func (a ByName) Len() int {
    return len(a)
}

// Swap function for sorting
func (a ByName) Swap(i, j int) {
    a[i], a[j] = a[j], a[i]
}

// Less function for sorting
func (a ByName) Less(i, j int) bool {
    return a[i].State < a[j].State
}
